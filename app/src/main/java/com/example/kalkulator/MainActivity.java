package com.example.kalkulator;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import org.mozilla.javascript.Context;
import org.mozilla.javascript.Scriptable;

public class MainActivity extends AppCompatActivity {
    Button btnHapus, btnBack, btnPersen, btnBagi,btnKali, btn9, btn8, btn7,btnMin,btn6, btn5, btn4,btnTambah, btn3, btn2,btn1, btnTitik, btn0, btnHasil;
    TextView angkaMasuk, angkaKeluar;
    String process;

    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btn0 = findViewById(R.id.btn0);
        btn1 = findViewById(R.id.btn1);
        btn2 = findViewById(R.id.btn2);
        btn3 = findViewById(R.id.btn3);
        btn4 = findViewById(R.id.btn4);
        btn5 = findViewById(R.id.btn5);
        btn6 = findViewById(R.id.btn6);
        btn7 = findViewById(R.id.btn7);
        btn8 = findViewById(R.id.btn8);
        btn9 = findViewById(R.id.btn9);

        btnTambah = findViewById(R.id.btnTambah);
        btnMin = findViewById(R.id.btnMin);
        btnBagi = findViewById(R.id.btnBagi);
        btnKali = findViewById(R.id.btnKali);

        btnHasil = findViewById(R.id.btnHasil);

        btnHapus = findViewById(R.id.btnHapus);
        btnTitik = findViewById(R.id.btnTitik);
        btnPersen = findViewById(R.id.btnPersen);
        btnBack = findViewById(R.id.btnBack);

        angkaMasuk = findViewById(R.id.angkaMasuk);
        angkaKeluar = findViewById(R.id.angkaKeluar);

        btnHapus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                angkaMasuk.setText("");
                angkaKeluar.setText("");
            }
        });

        btn0.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                process = angkaMasuk.getText().toString();
                angkaMasuk.setText(process + 0);
            }
        });

        btn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                process = angkaMasuk.getText().toString();
                angkaMasuk.setText(process + 1);
            }
        });

        btn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                process = angkaMasuk.getText().toString();
                angkaMasuk.setText(process + 2);
            }
        });

        btn3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                process = angkaMasuk.getText().toString();
                angkaMasuk.setText(process + 3);
            }
        });

        btn4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                process = angkaMasuk.getText().toString();
                angkaMasuk.setText(process + 4);
            }
        });

        btn5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                process = angkaMasuk.getText().toString();
                angkaMasuk.setText(process + 5);
            }
        });

        btn6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                process = angkaMasuk.getText().toString();
                angkaMasuk.setText(process + 6);
            }
        });

        btn7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                process = angkaMasuk.getText().toString();
                angkaMasuk.setText(process + 7);
            }
        });

        btn8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                process = angkaMasuk.getText().toString();
                angkaMasuk.setText(process + 8);
            }
        });

        btn9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                process = angkaMasuk.getText().toString();
                angkaMasuk.setText(process + 9);
            }
        });

        btnTambah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                process = angkaMasuk.getText().toString();
                angkaMasuk.setText(process + "+");
            }
        });

        btnMin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                process = angkaMasuk.getText().toString();
                angkaMasuk.setText(process + "-");
            }
        });

        btnKali.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                process = angkaMasuk.getText().toString();
                angkaMasuk.setText(process + "x");
            }
        });

        btnBagi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                process = angkaMasuk.getText().toString();
                angkaMasuk.setText(process + "/");
            }
        });

        btnTitik.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                process = angkaMasuk.getText().toString();
                angkaMasuk.setText(process + ".");
            }
        });

        btnPersen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                process = angkaMasuk.getText().toString();
                angkaMasuk.setText(process + "%");
            }
        });

        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String word = angkaMasuk.getText().toString();
                int input = word.length();
                if(input>0){
                    angkaMasuk.setText(word.substring(0, input-1));
                }
            }
        });

        btnHasil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                process = angkaMasuk.getText().toString();
                process = process.replaceAll("x", "*");
                process = process.replaceAll("%", "/100");
                process = process.replaceAll("/", "/");

                Context rhino = Context.enter();
                rhino.setOptimizationLevel(-1);
                String finalResult = "";

                try{
                    Scriptable scriptable = rhino.initSafeStandardObjects();
                    finalResult = rhino.evaluateString(scriptable, process, "javascript", 1, null).toString();
                } catch (Exception a){
                    finalResult = "0";
                }
                angkaKeluar.setText(finalResult);
                Intent pindah = new Intent(MainActivity.this, MainActivity2.class);
                pindah.putExtra("hasilakhir",finalResult);
                startActivity(pindah);
            }
        });
    }
}